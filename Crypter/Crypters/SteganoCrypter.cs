﻿using Cryptographer;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Text;

namespace Crypter.Crypters;

public class SteganoCrypter(byte[] data): ICrypter<byte[], byte[]>
{
    public virtual byte[] Encrypt(byte[] decrypted)
    {
        if (decrypted.Length * 8 > data.Length)
            throw new ApplicationException("Data is too small to fit message");

        byte[] result = data;

        var bitArray = new BitArray(data);

        int i = -1;
        foreach(byte b in decrypted)
        {
            bitArray.Set(++i * 8, (b & 0x01) == 0x01);
            bitArray.Set(++i * 8, (b & 0x02) == 0x02);
            bitArray.Set(++i * 8, (b & 0x04) == 0x04);
            bitArray.Set(++i * 8, (b & 0x08) == 0x08);
            bitArray.Set(++i * 8, (b & 0x10) == 0x10);
            bitArray.Set(++i * 8, (b & 0x20) == 0x20);
            bitArray.Set(++i * 8, (b & 0x40) == 0x40);
            bitArray.Set(++i * 8, (b & 0x80) == 0x80);
        }

        bitArray.CopyTo(result, 0);

        return result;
    }

    public virtual byte[] Decrypt(byte[] encrypted)
    {
        var bitArray = new BitArray(encrypted.Length);

        int i = -1;
        foreach (byte b in encrypted)
            bitArray.Set(++i, (b & 0x01) == 0x01);

        var result = new byte[encrypted.Length / 8];
        bitArray.CopyTo(result, 0);
        return result;
    }
}

public class SteganoStringToImage : ICrypter<string, Image<Rgb24>>
{
    private Image<Rgb24> image;
    public SteganoStringToImage(Image<Rgb24> image) =>
        this.image = image;

    public SteganoStringToImage() =>
        image = null!;

    public Image<Rgb24> Encrypt(string decrypted)
    {
        if (image == null)
            throw new ArgumentNullException("Image to fit message is not set");

        byte[] pixelBytes = new byte[image.Width * image.Height * Unsafe.SizeOf<Rgb24>()];
        image.CopyPixelDataTo(pixelBytes);
        var stegano = new SteganoCrypter(pixelBytes);
        var cryptedBytes = stegano.Encrypt(Encoding.Unicode.GetBytes(decrypted));
        var image_with_message = Image.LoadPixelData<Rgb24>(cryptedBytes, image.Width, image.Height);
        return image_with_message;
    }

    public string Decrypt(Image<Rgb24> image)
    {
        var cryptedBytes = new byte[image.Width * image.Height * Unsafe.SizeOf<Rgba32>()];
        image.CopyPixelDataTo(cryptedBytes);
        var stegano = new SteganoCrypter(Array.Empty<byte>());

        var encryptedBytes = stegano.Decrypt(cryptedBytes);
        return Encoding.Unicode.GetString(encryptedBytes);
    }
}
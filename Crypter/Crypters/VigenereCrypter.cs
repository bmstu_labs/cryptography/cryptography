﻿using Crypter;
using System.Text;

namespace Cryptographer.Crypters;


public class VigenereCrypter : ICrypter<string, string>
{
    private string key;
    private List<char> symbols;

    public VigenereCrypter(string key, char[] alphabet)
    {
        if (string.IsNullOrEmpty(key))
            throw new ArgumentException("Empty key");

        this.key = key;
        symbols = alphabet.ToList();
}

    private static int FloorMod(int x, int y) => x % y + (x % y < 0 ? y : 0);

    private bool CheckAlphabet(string s) => 
        !s.Union(key).Except(symbols).Except(Alphabets.SpecSymbols).Any();

    private string ExtendedKey(long length)
    {
        if (key.Length >= length)
            return key;

        StringBuilder sb = new();
        sb.Append(key);

        long l = key.Length;
        while (l < length)
        {
            sb.Append(key);
            l += key.Length;
        }

        return sb.ToString();
    }

    public string Encrypt(string decrypted)
    {
        if (!CheckAlphabet(decrypted))
            throw new ApplicationException("Some text or key symbols are absent in alphabet");

        char[] ca = new char[decrypted.Length];
        var extKey = ExtendedKey(decrypted.Length);

        for (int i = 0; i < decrypted.Length; i++)
            ca[i] = Alphabets.SpecSymbols.Contains(decrypted[i]) ? decrypted[i]
                : symbols[
                (symbols.IndexOf(decrypted[i]) + symbols.IndexOf(extKey[i]))
                    % symbols.Count];

        return new string(ca);
    }

    public string Decrypt(string encrypted)
    {
        if (!CheckAlphabet(encrypted))
            throw new ApplicationException("Some text or key symbols are absent in alphabet");

        char[] ca = new char[encrypted.Length];
        var extKey = ExtendedKey(encrypted.Length);

        for (int i = 0; i < encrypted.Length; i++)
            ca[i] = Alphabets.SpecSymbols.Contains(encrypted[i]) ? encrypted[i]
                : symbols[FloorMod(symbols.IndexOf(encrypted[i]) - symbols.IndexOf(extKey[i]),
                    symbols.Count)];

        return new string(ca);
    }
}

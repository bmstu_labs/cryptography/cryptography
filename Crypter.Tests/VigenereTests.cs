using Cryptographer.Crypters;

namespace Crypter.Tests;

public class VigenereTests
{
    [Theory]
    [InlineData("ATTACKATDAWN", "LEMON", "LXFOPVEFRNHR")]
    [InlineData("ATTACKATDAWN", "TEST", "TXLTVOSMWEOG")]
    public void Attackatdawn(string decrypted, string key, string knownEncrypted)
    {
        VigenereCrypter v = new(key, Alphabets.LatUpper);
        var encrepted = v.Encrypt(decrypted);
        Assert.Equal(knownEncrypted, encrepted);
        Assert.Equal(decrypted, v.Decrypt(encrepted));
    }
}
﻿using System.Collections;

namespace Cryptographer.Crypters;

public class StreamCrypter(IEnumerator<bool> gamma) : ICrypter<byte[], byte[]>
{
    public byte[] Decrypt(byte[] encrypted)
    {
        gamma.Reset();

        BitArray bitsDecrypted = new(encrypted);
        BitArray bitsCrypted = new(encrypted);

        byte[] result = new byte[encrypted.Length];

        int i = -1;
        foreach (var bit in bitsCrypted)
        {
            gamma.MoveNext();
            bitsDecrypted[++i] ^= gamma.Current;
        }

        bitsDecrypted.CopyTo(result, 0);

        return result;
    }

    public byte[] Encrypt(byte[] decrypted)
    {
        gamma.Reset();

        BitArray bitsDecrypted = new(decrypted);
        BitArray bitsCrypted = new(decrypted);

        byte[] result = new byte[decrypted.Length];

        int i = -1;
        foreach (var bit in bitsDecrypted)
        {
            gamma.MoveNext();
            bitsCrypted[++i] ^= gamma.Current;
        }

        bitsCrypted.CopyTo(result, 0);

        return result;
    }
}
﻿using Crypter;

namespace Cryptographer.Crypters;


public class CesarCrypter(int key, char[] alphabet) : ICrypter<string, string>
{
    private static int FloorMod(int x, int y) => x % y + (x % y < 0 ? y : 0);

    public CesarCrypter(int key) : this(key, Array.Empty<char>()) { }

    public int Key
    {
        get => key;
        set
        {
            if (key < 0)
                throw new ArgumentOutOfRangeException(nameof(key));

            key = value;
        }
    }

    private List<char> getAlphabet(string s)
    {
        var symbols = alphabet.Length == 0
            ? s.Distinct().Except(Alphabets.SpecSymbols).OrderBy(x => (int)x).ToList()
            : alphabet.ToList();

        if (s.Except(symbols).Except(Alphabets.SpecSymbols).Any())
            throw new ApplicationException("Some text symbols are absent in alphabet");

        return symbols;
    }

    public string Decrypt(string encrypted)
    {
        var symbols = getAlphabet(encrypted);

        if (key > symbols.Count - 1)
            throw new ArgumentOutOfRangeException(nameof(key));

        char[] ca = new char[encrypted.Length];

        for (int i = 0; i < encrypted.Length; i++)
            ca[i] = Alphabets.SpecSymbols.Contains(encrypted[i]) ? encrypted[i]
                : symbols[FloorMod(symbols.IndexOf(encrypted[i]) - key, symbols.Count)];

        return new string(ca);
    }

    public string Encrypt(string decrypted)
    {
        var symbols = getAlphabet(decrypted);

        if (key > symbols.Count - 1)
            throw new ArgumentOutOfRangeException(nameof(key));

        char[] ca = new char[decrypted.Length];

        for (int i = 0; i < decrypted.Length; i++)
            ca[i] = Alphabets.SpecSymbols.Contains(decrypted[i]) ? decrypted[i]
                : symbols[(symbols.IndexOf(decrypted[i]) + key) % symbols.Count];

        return new string(ca);
    }
}
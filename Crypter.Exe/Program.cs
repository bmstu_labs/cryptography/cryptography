﻿using Crypter.Crypters;
using Cryptographer.Crypters;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using System.Numerics;
using System.Text;

namespace Crypter.Exe;

internal class Program
{
    static void Main(string[] args)
    {
        void DemoLFSR()
        {
            var lfsr1 = new LFSR("01000101", l => l.Current);
            var lfsr2 = new LFSR("01000101", l => l[1] ^ l[2]);
            var lfsr3 = new LFSR("01000101", l => lfsr1.GetNext() ^ lfsr2.GetNext());

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Step {i}");
                Console.WriteLine(lfsr1);
                Console.WriteLine(lfsr2);
                Console.WriteLine(lfsr3);

                lfsr1.MoveNext();
                lfsr2.MoveNext();
                lfsr3.MoveNext();
            }

            lfsr1.Reset();
            lfsr2.Reset();

            var stream = new StreamCrypter(lfsr3);
            var M = "Hello, world! How are you?";
            var C = stream.Encrypt(Encoding.UTF8.GetBytes(M));
            Console.WriteLine(Encoding.UTF8.GetString(C));

            lfsr1.Reset();
            lfsr2.Reset();

            Console.WriteLine(Encoding.UTF8.GetString(stream.Decrypt(C)));
        }

        void DemoStaganography()
        {
            SteganoStringToImage cryptor = new(Image.Load<Rgb24>("c:\\1\\source.bmp"));
            string decripted = @"I
«Мой дядя самых честных правил,
Когда не в шутку занемог,
Он уважать себя заставил
И лучше выдумать не мог.
Его пример другим наука;
Но, боже мой, какая скука
С больным сидеть и день и ночь,
Не отходя ни шагу прочь!
Какое низкое коварство
Полуживого забавлять,
Ему подушки поправлять,
Печально подносить лекарство,
Вздыхать и думать про себя:
Когда же черт возьмет тебя!»
II
Так думал молодой повеса,
Летя в пыли на почтовых,
Всевышней волею Зевеса
Наследник всех своих родных.
Друзья Людмилы и Руслана!
С героем моего романа
Без предисловий, сей же час
Позвольте познакомить вас:
Онегин, добрый мой приятель,
Родился на брегах Невы,
Где, может быть, родились вы
Или блистали, мой читатель;
Там некогда гулял и я:
Но вреден север для меня 1.
III
Служив отлично благородно,
Долгами жил его отец,
Давал три бала ежегодно
И промотался наконец.
Судьба Евгения хранила:
Сперва Madame за ним ходила,
Потом Monsieur ее сменил.
Ребенок был резов, но мил.
Monsieur l'Abbé, француз убогой,
Чтоб не измучилось дитя,
Учил его всему шутя,
Не докучал моралью строгой,
Слегка за шалости бранил
И в Летний сад гулять водил.";
            var ImageWithMsg = cryptor.Encrypt(decripted);
            ImageWithMsg.SaveAsBmp("c:\\1\\modified.bmp");

            SteganoStringToImage decryptor = new();
            var decrypted_msg = decryptor.Decrypt(Image.Load<Rgb24>("c:\\1\\modified.bmp"));

            Console.WriteLine(decrypted_msg);
        }

        void DemoRSA()
        {
            var p = BigInteger.Parse("3490529510847650949147849619903898133417764638493387843990820577");
            var q = BigInteger.Parse("32769132993266709549961988190834461413177642967992942539798288533");
            p = BigInteger.Parse("769291017188670481713888105903950415664361860849840012932419644972460908311842473687262532928913341590312147583483990853810112210593149694485277763065491");
            q = BigInteger.Parse("9369098798265464920148156286181624247629011535102402423292377078620556230820451657206922925967917221732473982055604627478918173788491793589095464275722179");

            var e = 9007;

            //var ttt = RSA.GetFullPrivateParameters(p, q, e, 1);

            var keys = RsaTextCryptor.GenerateKeys(p, q, e);

            var rsa = new RsaTextCryptor(keys.PublicKey);
            var decrypted = "THE MAGIC WORDS ARE SQUEAMISH OSSIFRAGE";
            var encrypted = rsa.Encrypt(decrypted);

            Console.WriteLine(decrypted);
            Console.WriteLine(encrypted);
            rsa.Key = keys.PrivateKey;
            Console.WriteLine(rsa.Decrypt(encrypted));


            keys = RsaTextCryptor.GenerateKeys(512, e);
            rsa = new RsaTextCryptor(keys.PublicKey);
            decrypted = "THE MAGIC WORDS ARE SQUEAMISH OSSIFRAGE";
            encrypted = rsa.Encrypt(decrypted);

            Console.WriteLine(decrypted);
            Console.WriteLine(encrypted);
            rsa.Key = keys.PrivateKey;
            Console.WriteLine(rsa.Decrypt(encrypted));
        }

        void DemoCesar()
        {
            var c = new CesarCrypter(3, Alphabets.LatFull);
            var decrypted = "Hello, world!";
            Console.WriteLine(decrypted);
            
            var encrypted = c.Encrypt(decrypted);
            Console.WriteLine(encrypted);

            Console.WriteLine(c.Decrypt(encrypted));
        }

        void DemoVigenere()
        {
            var v = new VigenereCrypter("Hello, world!", Alphabets.LatFull);
            var decrypted = "Hello, world!";
            Console.WriteLine(decrypted);

            var encrypted = v.Encrypt(decrypted);
            Console.WriteLine(encrypted);

            Console.WriteLine(v.Decrypt(encrypted));
        }

        DemoCesar();
        DemoVigenere();
        DemoRSA();
        DemoLFSR();
        DemoStaganography();
    }
}

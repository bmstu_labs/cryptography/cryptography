using Cryptographer.Crypters;

namespace Crypter.Tests;

public class CesarTests
{
    [Fact]
    public void HelloAutoAlphabet()
    {
        var decrypted = "hello";
        CesarCrypter c = new(1);
        var encrepted = c.Encrypt(decrypted);
        Assert.Equal("lhooe", encrepted);

        c.Key = 2;
        encrepted = c.Encrypt(decrypted);
        Assert.Equal("oleeh", encrepted);
    }

    [Theory]
    [InlineData("������ � �������� ������!")]
    [InlineData("������, ������ ��� ��������, ������� �� ������ ����, ���, ����� ����, �������� ��")]
    [InlineData("�-��, ��� �������, ��, ��, ��")]
    [InlineData("c���� �� ��� ���� ������ ����������� ����� �� ����� ���")]
    public void ReverseConversionAutoAlphabet(string decrypted)
    {
        CesarCrypter c = new(1);

        var qnt = decrypted.ToCharArray().Except(Alphabets.SpecSymbols).Distinct().Count();
        for (int i = 1; i < qnt; i++)
        {
            c.Key = i;
            var encrepted = c.Encrypt(decrypted);
            Assert.NotEqual(decrypted, encrepted);
            Assert.Equal(c.Decrypt(encrepted), decrypted);
        }
    }

    [Fact]
    public void FrenchToastRusAlphabet()
    {
        var decrypted = "����� �� ��� ���� ������ ����������� ����� �� ����� ���";
        
        var expectedEncrypted = "����� �� ��� ���� ����� ����������� ����� �� ����� ���";
        CesarCrypter c = new(3, Alphabets.RusLower);
        var encrepted = c.Encrypt(decrypted);
        Assert.Equal(expectedEncrypted, encrepted);
        
        Assert.Equal(decrypted, c.Decrypt(encrepted));
    }
}
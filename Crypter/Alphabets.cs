﻿namespace Crypter;

public static class Alphabets
{
    public static char[] Digits => [.. "0123456789"];
    public static char[] RusUpper => [.. "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"];
    public static char[] RusLower => [.. "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"];
    public static char[] LatUpper => [.. "ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
    public static char[] LatLower => [.. "abcdefghijklmnopqrstuvwxyz"];

    public static char[] SpecSymbols = [.. " .,!"];

    public static char[] RusFull => RusUpper.Union(RusLower).ToArray();
    public static char[] LatFull => LatUpper.Union(LatLower).ToArray();
}
using Crypter.Crypters;
using System.Numerics;

namespace Crypter.Tests;

public class RsaTests
{
    [Fact]
    public void Test21()
    {
        RsaKey publicKey = new(691, 779);
        RsaKey privateKey = new(571, 779);
        var decrypted = 21;
        var knownEncrypted = BigInteger.Parse("717");
        var encrypted = new RsaNumberCryptor(publicKey).Encrypt(decrypted);
        Assert.Equal(knownEncrypted, encrypted);
        Assert.Equal(decrypted, new RsaNumberCryptor(privateKey).Decrypt(encrypted));
    }

    [Fact]    
    public void TestMagicWordsClassic()
    {
        RsaKey publicKey = new(exhibitor: 9007, modulus: BigInteger.Parse("114381625757888867669235779976146612010218296721242362562561842935706935245733897830597123563958705058989075147599290026879543541"));
        RsaKey privateKey = new(exhibitor: BigInteger.Parse("106698614368578024442868771328920154780709906633937862801226224496631063125911774470873340168597462306553968544513277109053606095"), modulus: BigInteger.Parse("114381625757888867669235779976146612010218296721242362562561842935706935245733897830597123563958705058989075147599290026879543541"));
        var decrypted = "THE MAGIC WORDS ARE SQUEAMISH OSSIFRAGE";
        var knownEncrypted = BigInteger.Parse("96869613754622061477140922254355882905759991124574319874695120930816298225145708356931476622883989628013391990551829945157815154");
        var encrypted = new RsaTextCryptor(publicKey).Encrypt(decrypted);
        Assert.Equal(knownEncrypted, encrypted);
        Assert.Equal(decrypted, new RsaTextCryptor(privateKey).Decrypt(encrypted));
    }

    [Fact]
    public void TestReverseRandomPQ512Length()
    {
        var e = 9007;
        for (int i = 0; i < 10; i++)
        {
            var keys = RsaTextCryptor.GenerateKeys(512, e);

            var rsa = new RsaTextCryptor(keys.PublicKey);
            var decrypted = "THE MAGIC WORDS ARE SQUEAMISH OSSIFRAGE";
            var encrypted = rsa.Encrypt(decrypted);
            rsa.Key = keys.PrivateKey;
            Assert.Equal(decrypted, rsa.Decrypt(encrypted));
        }
    }

    [Fact]
    public void TestReverseKnownPQ()
    {
        var p = BigInteger.Parse("769291017188670481713888105903950415664361860849840012932419644972460908311842473687262532928913341590312147583483990853810112210593149694485277763065491");
        var q = BigInteger.Parse("9369098798265464920148156286181624247629011535102402423292377078620556230820451657206922925967917221732473982055604627478918173788491793589095464275722179");
        var e = 9007;
        var keys = RsaTextCryptor.GenerateKeys(p, q, e);

        var rsa = new RsaTextCryptor(keys.PublicKey);
        var decrypted = "THE MAGIC WORDS ARE SQUEAMISH OSSIFRAGE";
        var encrypted = rsa.Encrypt(decrypted);

        Console.WriteLine(decrypted);
        Console.WriteLine(encrypted);
        rsa.Key = keys.PrivateKey;
        Console.WriteLine(rsa.Decrypt(encrypted));
    }

    [Theory]
    [InlineData("19", "41", "691", "571")]
    [InlineData("3490529510847650949147849619903898133417764638493387843990820577",
                "32769132993266709549961988190834461413177642967992942539798288533",
                "9007",
                "106698614368578024442868771328920154780709906633937862801226224496631063125911774470873340168597462306553968544513277109053606095")]
    public void TestKeyGeneration(string P, string Q, string E, string expectedD)
    {
        var p = BigInteger.Parse(P);
        var q = BigInteger.Parse(Q);
        var e = BigInteger.Parse(E);

        var keys = RsaCrypter<string, string>.GenerateKeys(p, q, e);
        Assert.Equal(new RsaKey(e, p*q), keys.PublicKey);

        var d = BigInteger.Parse(expectedD);
        Assert.Equal(new RsaKey(d, p * q), keys.PrivateKey);
    }
}
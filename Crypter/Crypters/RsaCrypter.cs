﻿using Cryptographer;
using PrimeNumbers;
using System.Numerics;

namespace Crypter.Crypters;

public struct RsaKey(BigInteger exhibitor, BigInteger modulus)
{
    public BigInteger Exhibitor => exhibitor;
    public BigInteger Modulus => modulus;
}

public abstract class RsaCrypter<TDecrypted, TEncrypted>(RsaKey key) : ICrypter<TDecrypted, TEncrypted>
{
    public RsaKey Key
    {
        get => key;
        set => key = value;
    }

    public static (RsaKey PublicKey, RsaKey PrivateKey) GenerateKeys
        (BigInteger p, BigInteger q, BigInteger e)
    {
        BigInteger ModInverse(BigInteger u, BigInteger v)
        {
            BigInteger u1, u3, v1, v3, t1, t3, q;
            int iter;
            
            // Step X1. Initialise
            u1 = 1;
            u3 = u;
            v1 = 0;
            v3 = v;

            // Remember odd/even iterations
            iter = 1;
            
            // Step X2. Loop while v3 != 0
            while (v3 != 0)
            {
                // Step X3. Divide and "Subtract"
                q = u3 / v3;
                t3 = u3 % v3;
                t1 = u1 + q * v1;

                // Swap
                u1 = v1; v1 = t1; u3 = v3; v3 = t3;
                iter = -iter;
            }
            
            // Make sure u3 = gcd(u,v) == 1
            if (u3 != 1)
                return 0; // Error: No inverse exists
            
            // Ensure a positive result
            if (iter < 0)
                return v - u1;
            else
                return u1;
        }

        var n = p * q;
        var phi = (p - 1) * (q - 1);

        if ((e < 2) || (e > phi))
            throw new ArgumentException($"Public exhibitor must by in a range [3;{phi})");

        if (phi % e == 0)
            throw new ArgumentException("e^-1 mod phi is invalid");

        BigInteger d = ModInverse(e, phi);

        if (d == 0)
            throw new ArgumentException("No ModInverse exists for e");

        return (new RsaKey(e, n), new RsaKey(d, n));
    }

    public static (RsaKey PublicKey, RsaKey PrivateKey) GenerateKeys
        (int numberOfBits, BigInteger e)
    {
        var rnd = new Random();
        BigInteger p;
        BigInteger q;

        do
        {
            var pbi = rnd.NextBigInteger(numberOfBits);
            p = Primes.GetNextPrimeMiller(pbi);

            var qbi = rnd.NextBigInteger(numberOfBits);
            q = Primes.GetNextPrimeMiller(qbi);
        }
        while (p * q % e == 0);

        return GenerateKeys(p, q, e);
    }

    public abstract TEncrypted Encrypt(TDecrypted decrypted);

    public abstract TDecrypted Decrypt(TEncrypted encrypted);
}


public class RsaNumberCryptor(RsaKey key) : RsaCrypter<BigInteger, BigInteger>(key)
{
    public override BigInteger Encrypt(BigInteger decrypted) =>
        BigInteger.ModPow(decrypted, Key.Exhibitor, Key.Modulus);

    public override BigInteger Decrypt(BigInteger encrypted) =>
        BigInteger.ModPow(encrypted, Key.Exhibitor, Key.Modulus);
}

public class RsaTextCryptor(RsaKey key): RsaCrypter<string, BigInteger>(key)
{
    public override BigInteger Encrypt(string decrypted)
    {
        int j = 0;
        Dictionary<char, int> dic = Alphabets.LatUpper.ToDictionary(x => x, x => ++j);
        dic.Add(' ', 0);

        var s = decrypted.Aggregate("", (first, next) => $"{first}{dic[next].ToString("00")}");
        var bi = BigInteger.Parse(s);

        if (bi > Key.Modulus)
            throw new AggregateException($"Key modulus {Key.Modulus} is too small for the message");

        return BigInteger.ModPow(bi, Key.Exhibitor, Key.Modulus);
    }

    public override string Decrypt(BigInteger encrypted)
    {
        int j = 0;
        Dictionary<int, char> dic = Alphabets.LatUpper.ToDictionary(x => ++j, x => x);
        dic.Add(0, ' ');

        var s = BigInteger.ModPow(encrypted, Key.Exhibitor, Key.Modulus).ToString();

        string result = "";
        for (int i = 0; i < s.Length; i += 2)
            result += dic[int.Parse($"{s[i]}{s[i + 1]}")];

        return result;
    }
}

﻿namespace Cryptographer;

public interface IEncrypter<TDecrypted, TEncrypted>
{
    TEncrypted Encrypt(TDecrypted decrypted);
}

public interface IDecrypter<TEncrypted, TDecrypted>
{
    TDecrypted Decrypt(TEncrypted encrypted);
}

public interface ICrypter<TDecrypted, TEncrypted>: IEncrypter<TDecrypted, TEncrypted>,
    IDecrypter<TEncrypted, TDecrypted>
{   
}
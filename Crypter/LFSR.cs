﻿using System.Collections;
using System.Numerics;

namespace Crypter;

/// <summary>
/// linear feedback shift register
/// </summary>
/// <param name="startBits">start bit sequence</param>
/// <param name="feedback">boolean function used to generate next greatest bit</param>
public class LFSR(string startBits, Func<LFSR, bool> feedback) : IEnumerator<bool>
{
    public int Length { get; private set; } = startBits.Length;

    public BigInteger register { get; private set; } = startBits.Aggregate(BigInteger.Zero, (s, a) => (s << 1) + a - '0');

    public bool this[int bitNumber] => (register & (1 << bitNumber)) > 0;

    public override string ToString()
    {
        var bits = register;

        string ret = string.Empty;
        if (bits == 0)
            ret = "0";
        else
            while (bits != 0)
            {
                ret += (char)((bits & 1) + '0');
                bits >>= 1;
            }
        char[] chars = ret.ToCharArray();
        Array.Reverse(chars);
        return $"{string.Join("", chars).PadLeft(Length, '0')}:{(current == null ? " " : (Current ? "1" : "0"))}";
    }

    private bool? current;

    public bool Current => current ?? throw new ApplicationException("Register has not been started. User MoveNext() to start it.");

    object IEnumerator.Current => Current;

    public bool MoveNext()
    {
        current = this[0];

        if (feedback(this))
            register |= (1 << Length);

        register >>= 1;

        return true; // (bool)current!;
    }

    public bool GetNext()
    {
        MoveNext();
        return Current;
    }

    public void Reset()
    {
        current = null;
        register = startBits.Aggregate(BigInteger.Zero, (s, a) => (s << 1) + a - '0');
    }

    public void Dispose() {; }
}